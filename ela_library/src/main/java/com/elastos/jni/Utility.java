package com.elastos.jni;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public class Utility {

    static {
        System.loadLibrary("utility");
    }

    public static String generateMnemonic(String language, String words){
        return nativeGenerateMnemonic(language, words);
    }

    public static String getSinglePrivateKey(String jlanguage, String jmnemonic, String jwords, String jpassword){
        return nativeGetSinglePrivateKey(jlanguage, jmnemonic, jwords, jpassword);
    }

    public static String getSinglePublicKey(String jlanguage, String jmnemonic, String jwords, String jpassword){
        return nativeGetSinglePublicKey(jlanguage, jmnemonic, jwords, jpassword);
    }

    public static String getAddress(String jpublickey){
        return nativeGetAddress(jpublickey);
    }

    public static String getRawTransaction(String transaction){
        return nativeGenerateRawTransaction(transaction);
    }

    private static native String nativeGenerateMnemonic(String jlanguage, String jwords);

    private static native String nativeGetSinglePrivateKey(String jlanguage, String jmnemonic, String jwords, String jpassword);

    private static native String nativeGetSinglePublicKey(String jlanguage, String jmnemonic, String jwords, String jpassword);

    private static native String nativeGetAddress(String jpublickey);

    private static native String nativeGenerateRawTransaction(String jtransaction);
}
