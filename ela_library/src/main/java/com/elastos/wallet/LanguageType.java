package com.elastos.wallet;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public interface LanguageType {
    String ENGLISH = "english";

    String SPANISH = "spanish";

    String FRENCH = "french";

    String JAPANESE = "japanese";

    String CHINESE = "chinese";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ENGLISH, SPANISH, FRENCH, JAPANESE, CHINESE})
    @interface LanguageTypeAnnotation {

    }


}
