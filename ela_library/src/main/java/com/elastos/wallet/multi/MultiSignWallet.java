package com.elastos.wallet.multi;

import android.content.Context;

import com.elastos.wallet.IWallet;
import com.elastos.wallet.model.CoinType;
import com.elastos.wallet.model.RawTransaction;

public class MultiSignWallet extends IWallet {

    private static MultiSignWallet mInstance;

    private MultiSignWallet(){}

    public static MultiSignWallet getInstance(Context context){
        if (mInstance == null)
          mInstance = new MultiSignWallet();
        return mInstance;
    }

    public CoinType getCoinType(){
        return null;
    }

    public String getAddress(){
        return null;
    }

    public String getPublicKey(){
        return null;
    }

    public String signTransaction(String data, String password){
        return null;
    }

    public RawTransaction getRawTransaction(String data){
        return null;
    }
}
