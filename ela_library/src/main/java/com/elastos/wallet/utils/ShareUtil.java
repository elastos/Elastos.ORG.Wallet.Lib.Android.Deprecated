package com.elastos.wallet.utils;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public class ShareUtil {
    private static final String FILE_NAME = "elastos_share";

    public static void putValue(Context context, String key, Object value){
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if(value instanceof String)
            editor.putString(key, (String) value);
        if(value instanceof Integer)
            editor.putInt(key, (Integer) value);
        if(value instanceof Boolean)
            editor.putBoolean(key, (Boolean) value);
        if(value instanceof Long)
            editor.putLong(key, (Long) value);
        if(value instanceof Float)
            editor.putFloat(key, (Float) value);
        editor.commit();
    }

    public static Object getValue(Context context, String key, Object defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        if(defaultValue instanceof String)
            sp.getString(key, (String) defaultValue);
        if(defaultValue instanceof Integer)
            sp.getInt(key, (Integer) defaultValue);
        if(defaultValue instanceof Boolean)
            sp.getBoolean(key, (Boolean) defaultValue);
        if(defaultValue instanceof Long)
            sp.getLong(key, (Long) defaultValue);
        if(defaultValue instanceof Float)
            sp.getFloat(key, (Float) defaultValue);

        return null;
    }

    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear().commit();
    }

    public static void clearByKey(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.commit();
    }
}
