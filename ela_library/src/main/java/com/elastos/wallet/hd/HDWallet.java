package com.elastos.wallet.hd;

import android.content.Context;

import com.elastos.wallet.IWallet;
import com.elastos.wallet.model.CoinType;
import com.elastos.wallet.model.RawTransaction;

public class HDWallet extends IWallet {

    private static HDWallet mInstance;

    private HDWallet(){}

    public static HDWallet getInstance(Context context){
        if(mInstance == null)
            mInstance = new HDWallet();
        return mInstance;
    }

    public CoinType getCoinType(){
        return null;
    }

    public RawTransaction signTransaction(String data, String password){
        return null;
    }

    public String getAddress(int chain, int index){
        return null;
    }

    public String getPublicKey(int chain, int index){
        return null;
    }

}
