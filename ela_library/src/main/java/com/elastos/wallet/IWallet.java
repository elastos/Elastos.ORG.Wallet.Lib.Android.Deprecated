package com.elastos.wallet;

import android.content.Context;

import com.elastos.wallet.utils.MnemonicUtil;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public abstract class IWallet {

    protected static String mWords = null;

    protected String getWords(Context context) {
        if (mWords == null)
            mWords = MnemonicUtil.getWords(context, WalletManager.getLanguage());

        return mWords;
    }

}
