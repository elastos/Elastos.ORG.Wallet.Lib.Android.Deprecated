package com.elastos.wallet;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public interface WalletType {

    String SINGLE = "singleWallet";

    String HD = "hdWallet";

    String MULTISIGN = "multiSignWallet";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SINGLE, HD, MULTISIGN})
    @interface WalletTypeAnnotation {

    }
}
