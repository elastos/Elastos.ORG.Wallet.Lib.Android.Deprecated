package com.elastos.wallet.identity;

import com.elastos.wallet.hd.HDWallet;
import com.elastos.wallet.model.Did;
import com.elastos.wallet.multi.MultiSignWallet;

import java.util.List;

public class Identity {

    public boolean changePassword(String oldPass, String newPass) {
        return false;
    }

    public boolean verifyPassword(String password) {
        return false;
    }

    public HDWallet getWallet() {
        return null;
    }

    public MultiSignWallet getWallet(List<String> publicKey, String privateKey, int count) {
        return null;
    }

    public Did getDid(int index) {
        return null;
    }

    public Did createDid() {
        return null;
    }

}
