package com.elastos.wallet;


import android.content.Context;

import com.elastos.wallet.hd.HDWallet;
import com.elastos.wallet.multi.MultiSignWallet;
import com.elastos.wallet.single.SingleWallet;
import com.elastos.wallet.utils.ShareUtil;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public class WalletManager<T extends IWallet> {

    private static Context mContext;

    private static WalletManager mInstance = null;

    private static final String LANGUAGE_SHARE_KEY = "language";
    public static String getLanguage(){
        return (String) ShareUtil.getValue(mContext, LANGUAGE_SHARE_KEY, LanguageType.ENGLISH);
    }

    public static void setLanguage(@LanguageType.LanguageTypeAnnotation String type){
        ShareUtil.putValue(mContext, LANGUAGE_SHARE_KEY, type);
    }

    public T getWallet(@WalletType.WalletTypeAnnotation String type) {
        if(type.equals(WalletType.SINGLE)) return (T) SingleWallet.getInstance(mContext);
        if(type.equals(WalletType.HD)) return (T) HDWallet.getInstance(mContext);
        if(type.equals(WalletType.SINGLE)) return (T) MultiSignWallet.getInstance(mContext);
        return null;
    }


    private WalletManager(Context context){
        this.mContext = context;
    }

    public static WalletManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new WalletManager(context);
        }

        return mInstance;
    }

}
