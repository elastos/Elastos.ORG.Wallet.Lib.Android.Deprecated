package com.elastos.wallet.single;

import android.content.Context;

import com.elastos.jni.Utility;
import com.elastos.wallet.IWallet;
import com.elastos.wallet.WalletManager;
import com.elastos.wallet.keystore.ElaKeyStore;
import com.elastos.wallet.utils.StringUtil;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public class SingleWallet extends IWallet {

    private static SingleWallet mInstance;

    private Context mContext;

    private SingleWallet(Context context){
        this.mContext = context;
    }

    public static SingleWallet getInstance(Context context){
        if(mInstance == null){
            mInstance = new SingleWallet(context);
        }

        return mInstance;
    }

    private String mMnemonic;
    public String generateMnemonic(){
        mMnemonic = Utility.generateMnemonic(WalletManager.getLanguage(), getWords(mContext));
        if(!StringUtil.isNullOrEmpty(mMnemonic)){
            ElaKeyStore.putPhrase(mContext, mMnemonic.getBytes());
            mPublic = null;
            mAddress = null;
        }
        return mMnemonic;
    }

    private String mPublic;
    public String getPublicKey(){
        if(!StringUtil.isNullOrEmpty(mPublic) && !StringUtil.isNullOrEmpty(mMnemonic))
            mPublic = Utility.getSinglePublicKey(WalletManager.getLanguage(),
                    new String(ElaKeyStore.getPhrase(mContext)),
                    getWords(mContext), "");
        return mPublic;
    }

    private String mAddress;
    public String getAddress(){
        if(!StringUtil.isNullOrEmpty(mAddress))
            mAddress = Utility.getAddress(getPublicKey());
        return mAddress;
    }

    public String signTransaction(String data){
            return Utility.getRawTransaction(data);
    }
}
