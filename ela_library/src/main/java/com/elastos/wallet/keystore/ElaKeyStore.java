package com.elastos.wallet.keystore;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.UserNotAuthenticatedException;
import android.util.Base64;

import com.elastos.wallet.utils.BytesUtil;
import com.elastos.wallet.utils.ShareUtil;
import com.elastos.wallet.utils.StringUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * @Author xidaokun
 * @Date 18-11-4
 */
public class ElaKeyStore {

    public static final String ANDROID_KEY_STORE = "AndroidKeyStore";
    public static final String NEW_CIPHER_ALGORITHM = "AES/GCM/NoPadding";
    private static final String PHRASE_FILENAME = "my_phrase";
    private static final String PHRASE_IV = "ivphrase";
    public static final String PHRASE_ALIAS = "phrase";
    public static final String CANARY_ALIAS = "canary";

    public static Map<String, AliasObject> aliasObjectMap;

    private static final ReentrantLock lock = new ReentrantLock();

    static {
        aliasObjectMap = new HashMap<>();
        aliasObjectMap.put(PHRASE_ALIAS, new AliasObject(PHRASE_ALIAS, PHRASE_FILENAME, PHRASE_IV));

//        Assert.assertEquals(aliasObjectMap.size(), 1);

    }

    public static boolean putPhrase(Context context, byte[] strToStore)  {
        AliasObject obj = aliasObjectMap.get(PHRASE_ALIAS);
        return !(strToStore == null || strToStore.length == 0) && putData(context, strToStore, obj.alias, obj.datafileName, obj.ivFileName, true);
    }

    public static byte[] getPhrase(Context context) {
        AliasObject obj = aliasObjectMap.get(PHRASE_ALIAS);
        return getData(context, obj.alias, obj.datafileName, obj.ivFileName);
    }

    private synchronized static boolean putData(Context context, byte[] data, String alias, String alias_file, String alias_iv, boolean auth_required) {

        validateSet(data, alias, alias_file, alias_iv, auth_required);
        KeyStore keyStore = null;
        try {
            lock.lock();
            keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            keyStore.load(null);
            SecretKey secretKey = (SecretKey) keyStore.getKey(alias, null);
            Cipher inCipher = Cipher.getInstance(NEW_CIPHER_ALGORITHM);

            if (secretKey == null) {
                secretKey = createKeys(alias, auth_required);
                inCipher.init(Cipher.ENCRYPT_MODE, secretKey);
            } else {
                try {
                    inCipher.init(Cipher.ENCRYPT_MODE, secretKey);
                } catch (InvalidKeyException ignored) {
                    if (ignored instanceof UserNotAuthenticatedException) {
                        throw ignored;
                    }
                    secretKey = createKeys(alias, auth_required);
                    inCipher.init(Cipher.ENCRYPT_MODE, secretKey);
                }
            }

            byte[] iv = inCipher.getIV();
            if (iv == null) throw new NullPointerException("iv is null!");

            storeEncryptedData(context, iv, alias_iv);
            byte[] encryptedData = inCipher.doFinal(data);
            storeEncryptedData(context, encryptedData, alias);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            lock.unlock();
        }
    }

    public static final String CIPHER_ALGORITHM = "AES/CBC/PKCS7Padding";
    private static byte[] getData(Context context, String alias, String alias_file, String alias_iv) {
        validateGet(alias, alias_file, alias_iv);
        KeyStore keyStore = null;
        try {
            lock.lock();
            keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            keyStore.load(null);
            SecretKey secretKey = (SecretKey) keyStore.getKey(alias, null);

            byte[] encryptedData = retrieveEncryptedData(context, alias);
            if (encryptedData != null) {
                //new format data is present, good
                byte[] iv = retrieveEncryptedData(context, alias_iv);
                if (iv == null) {
                    throw new RuntimeException("iv is missing when data isn't: " + alias + " (Can't proceed, risking user's phrase! )");
                }
                Cipher outCipher;

                outCipher = Cipher.getInstance(NEW_CIPHER_ALGORITHM);
                outCipher.init(Cipher.DECRYPT_MODE, secretKey, new GCMParameterSpec(128, iv));
                try {
                    byte[] decryptedData = outCipher.doFinal(encryptedData);
                    if (decryptedData != null) {
                        return decryptedData;
                    }
                } catch (IllegalBlockSizeException | BadPaddingException e) {
                    e.printStackTrace();
                    throw new RuntimeException("failed to decrypt data: " + e.getMessage());
                }
            }

            //no new format data, get the old one and migrate it to the new format
            String encryptedDataFilePath = getFilePath(alias_file, context);
            if (secretKey == null) {
                //no such key, the key is just simply not there
                boolean fileExists = new File(encryptedDataFilePath).exists();
                if (!fileExists) {
                    return null;// file also not there, fine then
                }
                return null;
            }

            boolean ivExists = new File(getFilePath(alias_iv, context)).exists();
            boolean aliasExists = new File(getFilePath(alias_file, context)).exists();
            //cannot happen, they all should be present
            if (!ivExists || !aliasExists) {
                removeAliasAndDatas(keyStore, alias, context);
                //report it if one exists and not the other.
                if (ivExists != aliasExists) {
                    return null;
                } else {
                    return null;
                }
            }

            byte[] iv = readBytesFromFile(getFilePath(alias_iv, context));
            if (BytesUtil.isNullOrEmpty(iv))
                throw new RuntimeException("iv is missing for " + alias);
            Cipher outCipher = Cipher.getInstance(CIPHER_ALGORITHM);
            outCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
            CipherInputStream cipherInputStream = new CipherInputStream(new FileInputStream(encryptedDataFilePath), outCipher);
            byte[] result = BytesUtil.readBytesFromStream(cipherInputStream);
            if (result == null)
                throw new RuntimeException("Failed to read bytes from CipherInputStream for alias " + alias);

            //create the new format key
            SecretKey newKey = createKeys(alias, (alias.equals(PHRASE_ALIAS) || alias.equals(CANARY_ALIAS)));
            if (newKey == null)
                throw new RuntimeException("Failed to create new key for alias " + alias);
            Cipher inCipher = Cipher.getInstance(NEW_CIPHER_ALGORITHM);
            //init the cipher
            inCipher.init(Cipher.ENCRYPT_MODE, newKey);
            iv = inCipher.getIV();
            //store the new iv
            storeEncryptedData(context, iv, alias_iv);
            //encrypt the data
            encryptedData = inCipher.doFinal(result);
            //store the new data
            storeEncryptedData(context, encryptedData, alias);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        return null;
    }

    public static byte[] readBytesFromFile(String path) {
        byte[] bytes = null;
        FileInputStream fin = null;
        try {
            File file = new File(path);
            fin = new FileInputStream(file);
            bytes = BytesUtil.readBytesFromStream(fin);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    public synchronized static void removeAliasAndDatas(KeyStore keyStore, String alias, Context context) {
        if (StringUtil.isNullOrEmpty(alias)) return;
        try {
            keyStore.deleteEntry(alias);
            AliasObject iv = aliasObjectMap.get(alias);
            if (iv == null) return;

            destroyEncryptedData(context, alias);
            destroyEncryptedData(context, iv.ivFileName);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

    }

    public static void destroyEncryptedData(Context context, String name) {
        ShareUtil.clearByKey(context, name);

    }

    public static String getFilePath(String fileName, Context context) {
        String filesDirectory = context.getFilesDir().getAbsolutePath();
        return filesDirectory + File.separator + fileName;
    }

    public static byte[] retrieveEncryptedData(Context ctx, String name) {
        SharedPreferences pref = ctx.getSharedPreferences(KEY_STORE_PREFS_NAME, Context.MODE_PRIVATE);
        String base64 = pref.getString(name, null);
        if (base64 == null) return null;
        return Base64.decode(base64, Base64.DEFAULT);
    }


    private static void validateGet(String alias, String alias_file, String alias_iv) throws IllegalArgumentException {
        AliasObject obj = aliasObjectMap.get(alias);
        if (obj != null && (!obj.alias.equals(alias) || !obj.datafileName.equals(alias_file) || !obj.ivFileName.equals(alias_iv))) {
            String err = alias + "|" + alias_file + "|" + alias_iv + ", obj: " + obj.alias + "|" + obj.datafileName + "|" + obj.ivFileName;
            throw new IllegalArgumentException("keystore insert inconsistency in names: " + err);
        }

    }

    private static final String KEY_STORE_PREFS_NAME = "keyStorePrefs";

    public static void storeEncryptedData(Context context, byte[] data, String name) {
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        ShareUtil.putValue(context, KEY_STORE_PREFS_NAME, base64);
    }

    public static final String NEW_PADDING = KeyProperties.ENCRYPTION_PADDING_NONE;
    public static final String NEW_BLOCK_MODE = KeyProperties.BLOCK_MODE_GCM;
    public static final int AUTH_DURATION_SEC = 300;

    private static SecretKey createKeys(String alias, boolean auth_required) throws InvalidAlgorithmParameterException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);

        // Set the alias of the entry in Android KeyStore where the key will appear
        // and the constrains (purposes) in the constructor of the Builder
        keyGenerator.init(new KeyGenParameterSpec.Builder(alias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(NEW_BLOCK_MODE)
                .setUserAuthenticationRequired(auth_required)
                .setUserAuthenticationValidityDurationSeconds(AUTH_DURATION_SEC)
                .setRandomizedEncryptionRequired(false)
                .setEncryptionPaddings(NEW_PADDING)
                .build());
        return keyGenerator.generateKey();

    }

    private static void validateSet(byte[] data, String alias, String alias_file, String alias_iv, boolean auth_required) throws IllegalArgumentException {
        if (data == null) throw new IllegalArgumentException("keystore insert data is null");
        AliasObject obj = aliasObjectMap.get(alias);
        if (obj != null && (!obj.alias.equals(alias) || !obj.datafileName.equals(alias_file) || !obj.ivFileName.equals(alias_iv))) {
            String err = alias + "|" + alias_file + "|" + alias_iv + ", obj: " + obj.alias + "|" + obj.datafileName + "|" + obj.ivFileName;
            throw new IllegalArgumentException("keystore insert inconsistency in names: " + err);
        }

        if (auth_required)
            if (!alias.equals(PHRASE_ALIAS) && !alias.equals(CANARY_ALIAS))
                throw new IllegalArgumentException("keystore auth_required is true but alias is: " + alias);
    }

    public static class AliasObject {
        public String alias;
        public String datafileName;
        public String ivFileName;

        AliasObject(String alias, String datafileName, String ivFileName) {
            this.alias = alias;
            this.datafileName = datafileName;
            this.ivFileName = ivFileName;
        }

    }
}
