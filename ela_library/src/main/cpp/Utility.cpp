//
// Created by xidaokun on 18-9-26.
//

#include "include/Elastos.Wallet.Utility.h"
#include "include/Utility.h"


static jstring JNICALL nativeGenerateMnemonic(JNIEnv *env,jobject clazz, jstring jlanguage, jstring jwords){

    const char* language = env->GetStringUTFChars(jlanguage, NULL);
    const char* words = env->GetStringUTFChars(jwords, NULL);

    char* mnemonic = generateMnemonic(language, words);

    jstring ret = env->NewStringUTF(mnemonic);

    free(mnemonic);
    env->ReleaseStringUTFChars(jlanguage, language);
    env->ReleaseStringUTFChars(jwords, words);

    return ret;
}


static jstring JNICALL nativeGetSinglePrivateKey(JNIEnv *env,jobject clazz, jstring jlanguage, jstring jmnemonic, jstring jwords, jstring jpassword){

    const char* mnemonic = env->GetStringUTFChars(jmnemonic, NULL);
    const char* language = env->GetStringUTFChars(jlanguage, NULL);
    const char* words = env->GetStringUTFChars(jwords, NULL);
    const char* password = env->GetStringUTFChars(jpassword, NULL);

    void* seed;
    int seedLen = getSeedFromMnemonic(&seed, mnemonic, language, words, password);

    char* privateKey = getSinglePrivateKey(seed, seedLen);
    jstring ret = env->NewStringUTF(privateKey);

    free(privateKey);
    env->ReleaseStringUTFChars(jmnemonic, mnemonic);
    env->ReleaseStringUTFChars(jlanguage, language);
    env->ReleaseStringUTFChars(jwords, words);
    env->ReleaseStringUTFChars(jpassword, password);

    return ret;

}

static jstring JNICALL nativeGetSinglePublicKey(JNIEnv *env,jobject clazz, jstring jlanguage, jstring jmnemonic, jstring jwords, jstring jpassword){

    const char* mnemonic = env->GetStringUTFChars(jmnemonic, NULL);
    const char* language = env->GetStringUTFChars(jlanguage, NULL);
    const char* words = env->GetStringUTFChars(jwords, NULL);
    const char* password = env->GetStringUTFChars(jpassword, NULL);

    void* seed;
    int seedLen = getSeedFromMnemonic(&seed, mnemonic, language, words, password);

    char* publicKey = getSinglePublicKey(seed, seedLen);
    jstring ret = env->NewStringUTF(publicKey);

    free(publicKey);
    env->ReleaseStringUTFChars(jmnemonic, mnemonic);
    env->ReleaseStringUTFChars(jlanguage, language);
    env->ReleaseStringUTFChars(jwords, words);
    env->ReleaseStringUTFChars(jpassword, password);

    return ret;

}

static jstring JNICALL nativeGetAddress(JNIEnv *env,jobject clazz, jstring jpublickey){
    const char* publickey = env->GetStringUTFChars(jpublickey, NULL);

    char* address = getAddress(publickey);
    jstring ret = env->NewStringUTF(address);

    free(address);
    env->ReleaseStringUTFChars(jpublickey, publickey);

    return ret;
}

static jstring JNICALL nativeGenerateRawTransaction(JNIEnv *env,jobject clazz, jstring jtransaction){
    const char* transaction = env->GetStringUTFChars(jtransaction, NULL);

    char* rawTransaction = generateRawTransaction(transaction);
    jstring ret = env->NewStringUTF(rawTransaction);

    free(rawTransaction);
    env->ReleaseStringUTFChars(jtransaction, transaction);

    return ret;
}

static const JNINativeMethod gMethods[] = {
        {"nativeGenerateMnemonic", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void*)nativeGenerateMnemonic},
        {"nativeGetSinglePrivateKey", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void*)nativeGetSinglePrivateKey},
        {"nativeGetSinglePublicKey", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void*)nativeGetSinglePublicKey},
        {"nativeGetAddress", "(Ljava/lang/String;)Ljava/lang/String;", (void*)nativeGetAddress},
        {"nativeGenerateRawTransaction", "(Ljava/lang/String;)Ljava/lang/String;", (void*)nativeGenerateRawTransaction}
};

jint register_methods(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/elastos/jni/Utility",
                                    gMethods, NELEM(gMethods));
}


JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved){
    JNIEnv * env;
    if(vm->GetEnv((void **)&env,JNI_VERSION_1_6) != JNI_OK){
        return JNI_ERR;
    }

    register_methods(env);

    return JNI_VERSION_1_6;
}

int jniRegisterNativeMethods(JNIEnv* env, const char* className,
                             const JNINativeMethod* gMethods, int numMethods)
{
    jclass cls = env->FindClass(className);
    env->RegisterNatives(cls, gMethods, numMethods);

    return 0;
}