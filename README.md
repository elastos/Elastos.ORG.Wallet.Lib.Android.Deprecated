## Elastos.ORG.Wallet.Lib.Android
It is a blockchain wallet library. This library provides the relatively consistent API that allows you to manage your wallets and sign transactions in ELA chains simultaneously.

## Installation

Step 1. Add repository to your build file
Add it in your root build.gradle at the end of repositories:
```groovy
allprojects {
	repositories {
		jcenter()
	}
}
```

Step 2. Add the dependency
```
dependencies {
	implementation 'com.elastos.library:wallet-core:1.0.0'
}
```

## WalletManager API
### Create Wallet
```
//SingleWallet example
SingleWallet singleWallet = (SingleWallet) WalletManager.createWallet(context, password, WalletType.SINGLE);

//param3：wallet type
public interface WalletType {

    String SINGLE = "singleWallet";

    String HD = "hdWallet";

    String MULTISIGN = "multiSignWallet";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SINGLE, HD, MULTISIGN})
    @interface WalletTypeAnnotation {

    }
}

```
### Import Wallet By Mnemonic
```
WalletManager.importWalletByMnemonic(context, password, mnemonic, walletType);

```
### Import Wallet By File(TODO)
```
WalletManager.importWalletByFile(context, password, file, walletType);
```

### Export Wallet(TODO)
```
WalletManager.exportWallet(path);
```
### Remove Wallet
```
WalletManager.removeWallet();
```
### Change Password
```
WalletManager.changePassword(oldPass, newPassword);
```
## SingleWallet API
### Get PublicKey
```
SingleWallet.getPublicKey();
```
### Get Address
```
SingleWallet.getAddress();
```

### Sign Transaction
```
SingleWallet.signTransaction(data);
```

## HDWallet API（TODO）

## MutilSignWallet API（TODO）

## Did API（TODO）
